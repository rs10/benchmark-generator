import pandas as pd
import collections
import matplotlib.pyplot as plt
import numpy as np
import json
import csv
from graphical_models.initial_volume_estimator import Estimator
from helper.utils import read_json, save_dictionary_as_csv, topological_sort, make_ordered_dict_in_list
from google_trends.google_trends_wrapper import GoogleTrends
import random

def remove_NAs_from_dataset(dataframe):
    return dataframe.dropna()

def save_dataframe_as_csv(dataframe, file):
    dataframe.to_csv(file, index=True)

def read_dataset(file):
     return pd.read_csv(file, index_col=0, header=0)

def build_estimator_dataset_from_table(benchmark_values, estimator_table_file):


    estimator = Estimator(benchmark_values)
    estimator.read_estimator_table(estimator_table_file)

    table = estimator.get_estimator().T

    print(table)


class ConstraintsGenerator:
    def __init__(self, geo, use_proxies = False):
        self.constraints = list()
        self.geo = geo
        self.timeframe = 'today 5-y'
        self.proxies = use_proxies


    def rank_words(self, words_list, iteration = 0, max_iterations = 2):
        length_benchmark = len(words_list)

        # GT only allows max of 5 comparisons per request
        max_groups = int(length_benchmark / 5 + 1) if np.mod(length_benchmark, 5) != 0 else int(length_benchmark / 5)
        groups = list()
        n_groups = len(groups)
        x = iteration
        y = 5

        #1st iteration we always create the groups by picking the first 5 elements in words, put in a group, pick next 5
        # put in the second group...and so on until we reach the max of groups possible

        if iteration == 0:
            print(iteration)
            while ( n_groups < max_groups):
                split_words = words_list[x:y]

                #thre should be no group with only 1 word, so we steal it from the last element inserted in the last group
                if len(split_words) == 1:
                    split_words.append(groups[-1][-1])
                    groups[-1].pop()

                groups.append(split_words)
                x = y + 1

                if (y+5) <= length_benchmark:
                    y = x +5
                else:
                    y = length_benchmark
                n_groups +=1


            for group in groups:
                gt_instance = GoogleTrends()
                rank = gt_instance.get_ranked_words_by_average_interest(group, self.geo, self.timeframe)

                self.constraints.append(rank)

            iteration += 1

        # 2nd iteration we transverse the groups horizontally picking one element from each group
        if iteration == 1:
            print(iteration)
            new_groups = list()
            n_groups = len(new_groups)

            while (n_groups < max_groups):
                aux_new_groups = list()

                for i in range(0, len(self.constraints)):
                    if (len(self.constraints[i]) > n_groups):
                        aux_new_groups.append(self.constraints[i][n_groups])

                n_groups += 1
                new_groups.append(aux_new_groups)

            for group in new_groups:
                gt_instance = GoogleTrends()
                rank = gt_instance.get_ranked_words_by_average_interest(group, self.geo, self.timeframe)

                self.constraints.append(rank)


            iteration += 1

        # 3rd iteration and beyond we start with naive approach of picking random items from the list and go until
        # max_iterations is reached
        if iteration > 1:
            if iteration == max_iterations:
                return self.constraints
            else:
                group = random.sample(words_list, k=5 )
                gt_instance = GoogleTrends()
                rank = gt_instance.get_ranked_words_by_average_interest(group, self.geo, self.timeframe )
                print("\nRank:")
                print(rank)
                self.constraints.append(rank)
                self.rank_words(words_list, iteration+1, max_iterations)


def calculate_percentage_diff(estimated_value, benchmark_value):
    x = estimated_value * 100 / benchmark_value

    if (x > 100):
        return round(x-100, 2)
    else:
        return round(100-x, 2)

def create_quality_sets_from_dataset(dataset, benchmark_values, save_to_file = False, output = ''):
    sets = {}
    for word in benchmark_values.keys():
        subset_good_quality = dataset[(dataset['estimated_word'] == word) & (dataset['quality'] == 'GOOD')]
        sets[word] = subset_good_quality

        if save_to_file:

            with open(output, 'a') as f:
                writer = csv.writer(f)
                writer.writerow(["\n"])
                writer.writerow([word])
                writer.writerow(["\n\n"])
                sets[word].to_csv(f)

    return sets

def create_topological_sorting_list(words, max_iterations):
    cg = ConstraintsGenerator('US')
    cg.rank_words(words_list=words, iteration=0, max_iterations=max_iterations)

    rank = list()


    for sort in topological_sort(
            cg.constraints):
            rank = list(sort)

    return rank

class PythiaClustering:
    def __init__(self, benchmark, sets):
        self.benchmark = benchmark
        self.sets = sets
        self.clusters = list()


    def get_all_estimated_value_average(self):
        self.estimated_avg = {}
        for key, value in self.sets.items():
            self.estimated_avg[key] = round(value['estimated_value'].mean(),2)

        return self.estimated_avg

    def build_clusters(self, sets):
        groups = list()
        word_count = {}
        for key, value in sets.items():
            word_grouping = value['truthful_word'].tolist()
            if len(word_grouping) > 0:
                word_grouping.append(key)
                groups.append(word_grouping)
                #print(word_grouping)
                for word in word_grouping:
                    word_count[word] = word_count.get(word,0) + 1
    ### Now we know in how many sets each word appears and how those sets look like
    #NEXT STEP: For these sets only, for each word, find all the intersections and associate them to that word



        for word in list(self.benchmark.keys()):
            subset = [group for group in groups if word in group]
            if len(subset) > 0:
                if len(list(set.intersection(*map(set, subset)))) > 1:
                    high_order_cluster = list(set.intersection(*map(set, subset)))
                    if not high_order_cluster in self.clusters:
                        self.clusters.append(high_order_cluster)

    def get_clusters(self):
        return self.clusters

    def calculate_centroid_for_clusters(self, clusters):
        centroids = []

        for cluster in clusters:
            value = 0
            size = len(cluster)
            for word in cluster:
                if self.estimated_avg[word] != None:
                    value += self.estimated_avg[word]

            centroid = round(value/size, 2)
            centroids.append(centroid)

        return(centroids)

if __name__ == "__main__":

    benchmark_values = read_json('../json/us.json')
    benchmark_words = benchmark_values.keys()
    words = list(benchmark_words)
    topology = create_topological_sorting_list(words, 100)

    print(topology)

















