from pytrends.request import TrendReq
from pytrends.exceptions import ResponseError
from helper.utils import read_json
from proxies.proxies import EC2
from time import sleep
import urllib3
import socket


class GoogleTrends:
    """
    A wrapper class for all the functionalities that require accessing Google Trends Service
    """

    def __init(self, proxies):
        #not being used right now
        self.max_proxies = 30

    # Returns keyword_list sorted by the their sorting of average interest (Default descending order)

    def get_ranked_words_by_average_interest(self, keyword_list, geo='', timeframe='today 5-y',
                                             ascending_sort = False, use_proxies=False):

        gt = self.get_google_trends_data(keyword_list=keyword_list, geo=geo,
                                         timeframe=timeframe, use_proxies=use_proxies)
        interests = self.interest_over_time(gt)
        average = interests[keyword_list].mean()

        return (list(average.sort_values(ascending=ascending_sort).index))


    # Returns a pytrends.request.TrendReq object
    @staticmethod
    def get_google_trends_data(keyword_list, geo='', timeframe='today 5-y', use_proxies=False):
        backoff_factor = 0.1
        # Get settings for proxies
        print(keyword_list)
        if (use_proxies):
            settings = read_json("../json/config.json")
            ec2 = EC2(settings["aws"]["ec2_security_group_id"],
                      settings["aws"]["ec2_ami_id"],
                      settings["aws"]["ec2_key_pair_name"])

            proxy_available = False
            proxy_ip = ""

            while not proxy_available:
                for instance in ec2.get_instances():

                    if ec2.get_instance_state(instance) == 'stopped':
                        ec2.start_instance(instance)
                    elif ec2.get_instance_state(instance) == 'running':

                        proxy_ip = ec2.get_proxy(instance)



                        try:
                            gt = TrendReq(hl='en-US', tz=360, proxies=[proxy_ip], retries=2,
                                          backoff_factor=backoff_factor)
                            print("\n\nTrying with proxy {} and IP {}".format(ec2.get_instance_name(instance), proxy_ip))

                            gt.build_payload(kw_list=keyword_list, cat=0, timeframe=timeframe, geo=geo)
                            return gt
                        except ResponseError:
                            print("Google returned a response with code 400.Trying a different proxy...")
                            continue

                        except urllib3.exceptions.ConnectTimeoutError:
                            print("\nConnection time out while connecting to this proxy. \nStopping proxy and moving"
                                  "to next one\n")
                            ec2.stop_instance(instance)
                            continue

                        except socket.error as socketerror:
                            print("\nSocket Error while connecting to this proxy. \nStopping proxy and moving"
                                  " to next one\n", socketerror)
                            ec2.stop_instance(instance)
                            continue

                    else:
                        print("Instance {} - {} on a pending state".format(ec2.get_instance_name(instance), proxy_ip))
                        sleep(10)


        else:
            gt = TrendReq()
            try:
                gt.build_payload(kw_list=keyword_list, cat=0, timeframe=timeframe, geo=geo)
                return gt
            except ResponseError:
                raise

    @staticmethod
    def interest_over_time(gt):
        return gt.interest_over_time()

    @staticmethod
    def interest_by_region(gt):
        return gt.interest_by_region()

    @staticmethod
    def related_queries(gt):
        gt.related_queries()

if __name__ == "__main__":
    pair = ['ebay', 'instagram']
    geo = 'US'
    use_proxies = False

    gt = GoogleTrends.get_google_trends_data(keyword_list=pair, geo=geo, use_proxies=use_proxies)
    interest = GoogleTrends.interest_over_time(gt)
    print(interest)
