# benchmark-generator

We use this project to keep our benchmarks updated and to maintain its quality

## Private Requriements
This project includes private repositories. They are indicated by a git link instead of the name of the repository found in the `requirements.txt`. To be able to install these requirements you need a key on your machine that grants you access.

See https://docs.gitlab.com/ee/ssh/#ed25519-ssh-keys for a tutorial on how to set up the keys.
