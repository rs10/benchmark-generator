import json
from collections import OrderedDict
import csv


def read_json(file = "config.json"):
    """
     Load all settings from config file
    """
    with open(file) as f:
        data = json.load(f)
        return data

def save_dictionary_as_csv(dictionary, csv_file, csv_columns):
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in dictionary:
                writer.writerow(data)
    except IOError:
        print("I/O error")

def write_to_json(file, data, indent=4):
    with open(file, 'w') as outfile:
        json.dump(data, outfile, indent=indent)
class Color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

# We can definitely improve this function later on
def get_key(value, dictionary):
    """
    This is a helper function for finding the corresponding dictionary to an value and returns the key.

    Method: Calculate all distances from value to the dictionary values. Take the minimum distance. Check if distance is positiv
    then the given key is wanted key. If the distance is negative then the key is (key -1).
    Args:
        value: value a dict has to be found
        dctnry: dictionary to find the index

    Returns:

    """

    shortest_distance = 1000000000000
    short_key = 0
    ordered = list(dictionary.keys())
    ordered.sort()

    for idx, key in enumerate(ordered):
        distance = key - value
        if (value >= 10000001):
            short_key = 10000001
        elif (abs(distance) < shortest_distance):
            if (distance > 0):
                shortest_distance = abs(distance)
                short_key = key
            else:
                shortest_distance = abs(distance)
                short_key = ordered[idx + 1]

    return short_key


def first(s):
    """
    Return the first element from an ordered collection
    or an arbitrary element from an unordered collection.
    Raise StopIteration if the collection is empty.
    """
    return next(iter(s))


def make_ordered_dict_in_list(dictionary):
    """
    Makes an ordered dictionary out of all the normal (unordered) dictionaries inside a list
    """
    for i in range(len(dictionary)):
        dictionary[i] = OrderedDict(dictionary[i])
    return dictionary

def get_values_in_list_of_dicts(list_of_dicts, key, get_nones = True):
    """
    Returns a list of values for the corresponding key found in a list of dictionaries.
    None is returned in case key does not exist for a dictionary on parameter get_nones
    """
    res = [ sub.get(key, None) for sub in list_of_dicts]
    if get_nones:
        return res
    else:
        res = list(filter(None, res))
        return res


def delete_keys_from_dict(dict_del, lst_keys):
    """
       Remove keys that can be nested in any level in a dictionary
    """
    for k in lst_keys:
        try:
            del dict_del[k]
        except KeyError:
            pass
    for v in dict_del.values():
        if isinstance(v, dict):
            delete_keys_from_dict(v, lst_keys)

    return dict_del

def topological_sort(descending_chains):
    arrive_to = {}
    outstanding_arrivals = {}
    for chain in descending_chains:
        for x in chain:
            if x not in arrive_to:
                arrive_to[x] = set([])
                outstanding_arrivals[x] = 0
        for i in range(1, len(chain)):
            arrive_to[chain[i-1]].add(chain[i])

    for item in arrive_to:
        for x in arrive_to[item]:
            outstanding_arrivals[x] += 1

    todo = [['choose', None]]
    sorted_items = []
    chosen = set([])
    items = [x for x in arrive_to]
    while len(todo):
        action, item = todo.pop()
        if action == 'choose':
            choices = []
            for x in outstanding_arrivals:
                if x not in chosen and 0 == outstanding_arrivals[x]:
                    choices.append(x)
            if 0 == len(choices):
                if len(sorted_items) == len(items):
                    yield sorted_items
                else:
                    print((choices, outstanding_arrivals, sorted_items))
                    break
            else:
                for item in choices:
                    todo.append(['try', item])
        elif action == 'try':
            chosen.add(item)
            sorted_items.append(item)
            todo.append(['remove', item])
            todo.append(['choose', None])
            todo.append(['add', item])
        elif action == 'add':
            chosen.add(item)
            for x in arrive_to[item]:
                outstanding_arrivals[x] -= 1
        elif action == 'remove':
            chosen.remove(item)
            sorted_items.pop()
            for x in arrive_to[item]:
                outstanding_arrivals[x] += 1
        else:
            yield ('todo:', action, item)

if __name__ == "__main__":

    test_constraints = [['facebook', 'youtube', 'yahoo', 'ebay', 'bing'], ['craigslist', 'pornhub', 'twitter', 'msn', 'paypal'], ['pizza', 'chase', 'wikipedia', 'nytimes', 'aliexpress'], ['house', 'office', 'force', 'biographies'], ['light', 'hamburger'], ['facebook', 'craigslist', 'house', 'pizza', 'light'], ['youtube', 'office', 'pornhub', 'chase', 'hamburger'], ['yahoo', 'twitter', 'force', 'wikipedia'], ['ebay', 'msn', 'nytimes', 'biographies'], ['paypal', 'bing', 'aliexpress'], ['ebay', 'light', 'bing', 'nytimes', 'imgur'], ['house', 'pizza', 'instagram', 'twitter', 'chase'], ['facebook', 'youtube', 'wikipedia', 'bing', 'imgur'], ['facebook', 'youtube', 'pizza', 'instagram', 'ebay'], ['craigslist', 'office', 'paypal', 'hamburger', 'imgur'], ['yahoo', 'windows', 'force', 'aliexpress', 'biographies'], ['facebook', 'twitter', 'reddit', 'force', 'nytimes'], ['youtube', 'craigslist', 'ebay', 'aliexpress', 'imgur'], ['pizza', 'instagram', 'ebay', 'chase', 'nytimes'], ['yahoo', 'pizza', 'pornhub', 'bing', 'nytimes'], ['office', 'instagram', 'light', 'twitter', 'paypal'], ['reddit', 'msn', 'nytimes', 'imgur', 'biographies'], ['facebook', 'craigslist', 'pizza', 'chase', 'wikipedia'], ['facebook', 'house', 'instagram', 'ebay', 'nytimes'], ['facebook', 'office', 'instagram', 'chase', 'nytimes'], ['ebay', 'twitter', 'force', 'paypal', 'bing'], ['craigslist', 'house', 'hamburger', 'imgur', 'biographies'], ['craigslist', 'house', 'yahoo', 'ebay', 'bing'], ['office', 'windows', 'paypal', 'wikipedia', 'aliexpress'], ['office', 'instagram', 'windows', 'wikipedia', 'nytimes'], ['yahoo', 'instagram', 'light', 'paypal', 'bing'], ['facebook', 'ebay', 'twitter', 'bing', 'hamburger'], ['youtube', 'pornhub', 'windows', 'hamburger', 'imgur'], ['facebook', 'youtube', 'windows', 'nytimes', 'imgur'], ['facebook', 'house', 'office', 'pornhub', 'ebay'], ['ebay', 'force', 'wikipedia', 'hamburger', 'aliexpress'], ['youtube', 'twitter', 'windows', 'imgur', 'biographies'], ['youtube', 'ebay', 'light', 'nytimes', 'biographies'], ['office', 'msn', 'wikipedia', 'aliexpress', 'imgur'], ['craigslist', 'ebay', 'windows', 'paypal', 'imgur'], ['ebay', 'windows', 'msn', 'force', 'biographies'], ['craigslist', 'instagram', 'windows', 'paypal', 'biographies'], ['craigslist', 'house', 'light', 'chase', 'aliexpress'], ['twitter', 'chase', 'msn', 'nytimes', 'aliexpress'], ['house', 'pizza', 'light', 'reddit', 'windows'], ['pornhub', 'reddit', 'nytimes', 'aliexpress', 'imgur'], ['youtube', 'craigslist', 'office', 'bing', 'biographies'], ['house', 'yahoo', 'twitter', 'bing', 'aliexpress'], ['craigslist', 'pornhub', 'instagram', 'windows', 'bing'], ['yahoo', 'office', 'pornhub', 'paypal', 'nytimes'], ['youtube', 'reddit', 'bing', 'nytimes', 'imgur'], ['pornhub', 'reddit', 'chase', 'wikipedia', 'nytimes'], ['youtube', 'craigslist', 'office', 'light', 'twitter'], ['youtube', 'craigslist', 'house', 'office', 'paypal'], ['office', 'pornhub', 'instagram', 'hamburger', 'nytimes'], ['yahoo', 'pizza', 'force', 'hamburger', 'aliexpress'], ['yahoo', 'pornhub', 'hamburger', 'aliexpress', 'imgur'], ['ebay', 'force', 'bing', 'nytimes', 'aliexpress'], ['facebook', 'pornhub', 'light', 'windows', 'msn'], ['house', 'yahoo', 'pizza', 'bing', 'hamburger'], ['pizza', 'paypal', 'wikipedia', 'bing', 'hamburger'], ['facebook', 'pornhub', 'instagram', 'msn', 'wikipedia'], ['youtube', 'pornhub', 'chase', 'msn', 'force'], ['office', 'instagram', 'twitter', 'windows', 'aliexpress'], ['facebook', 'pornhub', 'ebay', 'msn', 'biographies'], ['craigslist', 'pizza', 'pornhub', 'light', 'msn'], ['craigslist', 'house', 'instagram', 'ebay', 'hamburger'], ['pornhub', 'ebay', 'light', 'msn', 'imgur'], ['facebook', 'ebay', 'msn', 'paypal', 'nytimes'], ['instagram', 'reddit', 'bing', 'nytimes', 'aliexpress'], ['facebook', 'craigslist', 'pizza', 'office', 'instagram'], ['pizza', 'office', 'msn', 'bing', 'nytimes'], ['facebook', 'office', 'wikipedia', 'bing', 'nytimes'], ['youtube', 'craigslist', 'yahoo', 'pizza', 'hamburger'], ['ebay', 'reddit', 'chase', 'msn', 'imgur'], ['facebook', 'ebay', 'windows', 'chase', 'biographies'], ['yahoo', 'office', 'ebay', 'nytimes', 'aliexpress'], ['facebook', 'twitter', 'reddit', 'windows', 'aliexpress'], ['house', 'pizza', 'paypal', 'wikipedia', 'hamburger'], ['facebook', 'youtube', 'pizza', 'office', 'paypal'], ['house', 'light', 'chase', 'force', 'nytimes'], ['youtube', 'yahoo', 'light', 'aliexpress', 'biographies'], ['craigslist', 'pizza', 'chase', 'force', 'paypal'], ['youtube', 'office', 'instagram', 'reddit', 'wikipedia'], ['house', 'pornhub', 'msn', 'bing', 'nytimes'], ['craigslist', 'ebay', 'reddit', 'chase', 'force'], ['youtube', 'ebay', 'twitter', 'windows', 'paypal'], ['craigslist', 'pornhub', 'chase', 'bing', 'biographies'], ['facebook', 'house', 'twitter', 'paypal', 'hamburger'], ['instagram', 'ebay', 'twitter', 'windows', 'imgur'], ['light', 'reddit', 'chase', 'bing', 'nytimes'], ['youtube', 'instagram', 'windows', 'chase', 'bing'], ['office', 'reddit', 'chase', 'paypal', 'wikipedia'], ['facebook', 'pizza', 'pornhub', 'twitter', 'chase'], ['pizza', 'office', 'light', 'chase', 'force'], ['house', 'light', 'reddit', 'wikipedia', 'bing'], ['ebay', 'twitter', 'msn', 'imgur', 'biographies'], ['facebook', 'house', 'pizza', 'nytimes', 'biographies'], ['youtube', 'craigslist', 'pizza', 'pornhub', 'windows'], ['house', 'pizza', 'reddit', 'paypal', 'biographies'], ['house', 'office', 'reddit', 'bing', 'hamburger'], ['youtube', 'yahoo', 'reddit', 'windows', 'nytimes'], ['facebook', 'youtube', 'instagram', 'wikipedia', 'hamburger'], ['youtube', 'yahoo', 'twitter', 'reddit', 'biographies'], ['office', 'instagram', 'aliexpress', 'imgur', 'biographies'], ['youtube', 'pizza', 'light', 'force', 'wikipedia'], ['yahoo', 'pornhub', 'reddit', 'paypal', 'hamburger'], ['office', 'pornhub', 'reddit', 'paypal', 'nytimes']]

    print("Topological Sort: ")
    for sort in topological_sort(
            test_constraints):
        print(sort)
