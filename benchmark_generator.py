from clustering.pythia_clustering import read_dataset, create_quality_sets_from_dataset, PythiaClustering
from helper.utils import read_json
from google_trends.google_trends_wrapper import GoogleTrends
# from swarm_mongo.controller import MongoController


class BenchmarkGenerator :
    """
       This Class Generates Benchmarks based on Pythia Clustering Method
    """

    def __init__(self, PC, benchmark, topology):
        self.benchmark = benchmark
        self.min_size_clusters = 3
        self.clustering = PC
        self.topology = topology
        self.geo = 'US'

    def calculate_estimate_by_proportion(self, word_lb, word, word_ub, value_lb, value_ub):

        gt_instance = GoogleTrends()
        gt = gt_instance.get_google_trends_data([word_lb, word, word_ub], self.geo)
        interests = gt_instance.interest_over_time(gt)

        average_ub = interests[word_ub].mean()
        average_lb = (value_lb * average_ub)/ value_ub
        average_word =  (average_ub + average_lb) / 2
        estimate_word = (value_lb * average_word) / average_lb


        return estimate_word


    def run(self):

        # Step 1: Select only clusters with size >= min_size_clusters and consider those words only for now.
        # Mark them as green:

        green_words = list()
        for cluster in clustering.get_clusters():
            if len(cluster) >= self.min_size_clusters:
                green_words.extend(cluster)
        green_words = list(dict.fromkeys(green_words))


        EAV = clustering.get_all_estimated_value_average()
        subset_green = {k: EAV[k] for k in green_words}

        topology_subset_green = sorted(set(self.topology).intersection(subset_green), key=lambda x: self.topology.index(x))


        # Step 2 : Define our upper and lower boundaries:
        # - The upper boundary value is the highest Estimated Value Average of the highest ranked green marked word
        # - The lower boundary value is the lowest  Estimated Value Average of the lowest ranked green marked word

        lower_boundary = min(subset_green, key=subset_green.get)


        # Step 3 : Correct E.A. V for the green words
        # we must follow the order set by topology list

        needs_sort = True

        while needs_sort:
            flag_change = 0
            for idx, word in enumerate(topology_subset_green):
                if idx > 0:
                    if subset_green[topology_subset_green[idx-1]]  < subset_green[topology_subset_green[idx-1]] and subset_green[word] < subset_green[lower_boundary]:
                        subset_green[word] = (subset_green[topology_subset_green[idx-1]] + subset_green[lower_boundary]) / 2
                        flag_change += 1

                    if subset_green[word] > subset_green[topology_subset_green[idx-1]]:
                        subset_green[topology_subset_green[idx-1]] = (subset_green[topology_subset_green[idx-2]] + subset_green[word]) / 2
                        flag_change += 1

            if flag_change == 0:
                needs_sort = False

        subset_green = {k: v for k, v in sorted(subset_green.items(), key=lambda item: item[1], reverse=True)}

        # Step 4 : Estimate values for words with no quality cluster
        # We go back to our original table and mark all the missing words as red
        # We make new estimated value averages just like in the previous step, but instead of median we take the same
        # proportion as GT gives us for their comparison
        topology_subset_red = sorted(set(self.topology).symmetric_difference(topology_subset_green), key=lambda x: self.topology.index(x))


        indexes_green = [i for i in range(len(self.topology)) if self.topology[i] in topology_subset_green]
        indexes_red = [i for i in range(len(self.topology)) if self.topology[i] in topology_subset_red]

        boundaries = []

        for index in indexes_red:
            if index >=1  and index != indexes_red[-1]:
                if len(boundaries) == 0:
                    lower_boundary = index -1
                else:
                    lb = next(i for i in reversed(indexes_green) if i < index)
                    if lb > boundaries[-1]:
                        lower_boundary = lb
                    else:
                        lower_boundary = boundaries.pop()
                try:
                    upper_boundary_index = next(x[0] for x in enumerate(indexes_green) if x[1] > index)
                except: break

                upper_boundary = indexes_green[upper_boundary_index]
                subset_green[self.topology[index]] = self.calculate_estimate_by_proportion(
                    self.topology[lower_boundary], self.topology[index],
                    self.topology[upper_boundary], subset_green[self.topology[lower_boundary]],
                    subset_green[self.topology[upper_boundary]])

                boundaries.append(index)
            if index == indexes_red[-1]:
                subset_green[self.topology[index]] =  subset_green[self.topology[index-1]]/2


        return subset_green

    def save_benchmarks_to_db(self, db, geo,  benchmarks):
        # mc = MongoController(db_name=db)

        for key, value in benchmarks.items():
            print(key,value)
            # mc.add_benchmark('US', key, value)




if __name__ == "__main__":
    benchmark_values = read_json('json/benchmarks_US_parsed.json')
    benchmark_words = benchmark_values.keys()
    words = list(benchmark_words)

    dataset = read_dataset('dataset/dataset.csv')

    sets = create_quality_sets_from_dataset(dataset, benchmark_values)

    #topology = create_topological_sorting_list(words, 100)
    real_topology = ['facebook', 'youtube', 'craigslist', 'house', 'yahoo', 'pizza', 'office', 'pornhub', 'instagram', 'ebay', 'light', 'twitter', 'reddit', 'windows', 'chase', 'msn', 'force', 'paypal', 'wikipedia', 'bing', 'hamburger', 'nytimes', 'aliexpress', 'imgur', 'biographies']


    clustering = PythiaClustering(benchmark_values, sets)
    clustering.build_clusters(sets)
    #print(clustering.get_clusters())
    #print(clustering.get_all_estimated_value_average())

    BG = BenchmarkGenerator(clustering, benchmark_values, real_topology)
    result_benchmarks = BG.run()
    BG.save_benchmarks_to_db(db='testDb', geo='US',benchmarks= result_benchmarks)

