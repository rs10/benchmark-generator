import pandas as pd
import itertools
from datetime import datetime
from scipy import stats
import plotly.offline as py
import plotly.graph_objs as go
from matplotlib import style
from matplotlib.pylab import rcParams
from graphical_models.word_approver_markov_chain_model import WordApprover
from google_trends.google_trends_wrapper import GoogleTrends

class BenchmarkValidator:
    """
       This Class generates and evaluate absolute values from Google Trends.
    """

    def __init__(self, benchmark_file):
        self.validation_threshold = 0.30

        self.benchmark = pd.read_json(benchmark_file, typ='frame') # Get the Benchmark json for checking their values
        self.json_as_dataframe = self.from_json_to_dataframe(self.benchmark)
        self.approved_benchmarks = set()
        self.disapproved_benchmarks = set()
        self.set_graphic_options()
        self.debug = False

    # best to pass a set of arguments to the function, but let's leave it like this for now
    @staticmethod
    def set_graphic_options(figsize_width=15, fig_size_height=6,
                            mpl_style='ggplot', display_max_row=100,
                            display_max_columns=50):

        rcParams['figure.figsize'] = figsize_width, fig_size_height
        style.use(mpl_style)
        pd.set_option('display.max_row', display_max_row)
        pd.set_option('display.max_columns', display_max_columns)

    @staticmethod
    def from_json_to_dataframe(dataframe):
        """
        This function removes the the results grouping axis ('TenMs', 'Millions', 'Million', 'HundredKs', 'HundredK',
        'TenK', 'Thousands' ...) and concatenate everything into one pandas df.
        Args:
            data: A pandas dataframe resulting from pd.read_json().

        Returns: A pandas dataframe without the scale axis.

        """
        result = pd.DataFrame()
        for idx, geo in enumerate(dataframe):
            tmp_data = dataframe[geo]
            # First drop every item with nan instead of an dict.
            dicts_list = [tmp_data[key] for key in tmp_data.keys()
                          if tmp_data.isnull().loc[key] == False]
            # Concatenate every item from the keys to achieve one column with all wanted data.
            geo_data = pd.DataFrame()
            geo_data = pd.concat(
                [pd.DataFrame.from_dict(key, orient='index') for key in dicts_list if type(key) != 'float'])
            result = pd.concat([result, geo_data], axis=1, ignore_index=True, sort=False)

        result.columns = dataframe.keys()

        return result

    def check_stability(self, words, interests, std_err_level=0.05, slope_level=0.1, geo=''):
        """
        Function checks given words for stability. If a given word doesn't result in a stable graph it will be dropped.

        Args:
            words:          List containing words to check.
            interests:      A list of results from the self.interest_over_time(gt)
            std_err_level:  Maximum allowed standard deviation error for measuring the variance.
            slope_level:    Maximum allowed slope of the regression line.
            geo:            Geo location where data points are coming from.

        Returns:   List of words holding the measure limits.

        """

        words_list = []
        for word in words:
            x = list(range(0, interests.index.size))
            slope, intercept, r_value, p_value, std_err = stats.linregress(x, interests[word])
            if slope_level >= abs(slope) and std_err_level >= abs(std_err) and interests[word].mean() > 0:
                words_list.append(word)
        # If you want to debug, this will create a figure of all given benchmarks.
        if (self.debug == True):
            name = './health_check_for_' + geo + '_' + datetime.now().strftime('%Y-%m-%d %H:%M:%S')+ '.html'
            self.print_health_check(words=words, interest=interests, filename_path=name)
        return words_list


    def health_check(self, data, slope=0.1, std_err=0.04, geos=[]):
        """
        Method for checking benchmark words holding still benchmark criteria.
        Criteria is measured with an linear regression. The std_err gives the error to this line and slope is the slope of
        this regression.

        Args:
            data:       Input data
            slope:      Slope level - data has to be lower than this.
            std_err:    Standard Error level - data has to be lower than this.
            geo:        If you want to check just a (or a selection of) geo location(s).
            debug:      If you want to get information and a plot of the results.

        Returns:        Data Frame in Benchmark.json format with health checked data.

        """
        # result = pd.DataFrame(columns=benchmark.columns, index=benchmark.index)
        result = data


        for geo in geos:
            data_geo = data[geo]
            data_geo.dropna(inplace=True)

            result_aux = pd.DataFrame(columns=data_geo.index)
            for idx, item in enumerate(data_geo.index):
                gt_instance = GoogleTrends()
                gt = gt_instance.get_google_trends_data([item], geo)
                interests = gt_instance.interest_over_time(gt)
                result_aux[item] = interests[item]


            interests_geo = result_aux
            checked_benchmarks = self.check_stability(words=list(data_geo.index), interests=interests_geo,
                                                      std_err_level=std_err, slope_level=slope, geo=geo)

            # set of item which has to be dropped.
            dropped_benchmarks = list(set(data_geo.index) - set(checked_benchmarks))

            print(f" For geo {geo} these words will be deleted: {dropped_benchmarks}")
            for item in dropped_benchmarks:
                data_geo.drop(item, inplace=True)

            index = result.columns.get_loc(geo)
            result.iloc[:, index] = data_geo
            result.dropna(inplace=True)

        return result


    def print_health_check(self, words, interest, filename_path='./Health_Check.html'):
        """
        Method for getting valuable information of deleted and non deleted benchmarks.
        Args:
            words:          List of words to investigate with.
            interest:       Corresponding Google Trends Relative Values.
            filename_path:  Name and path where to store the graph evaluation.

        Returns:

        """
        x = list(range(0, interest.index.size))
        y_dict = {}
        for word in words:
            y = interest[word]
            slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
            # print(f"Stats for fitting line for {word}: \n slope: {slope};  intercept: {intercept}; r_value: {r_value}; p_value: {p_value}; std_err: {std_err}")
            fitting_line = [value * slope + intercept for value in x]
            data_scatter = go.Scatter(x=x, y=interest[word], name=word)
            fit_scatter = go.Scatter(x=x, y=fitting_line, name=word + '_fitting_line')
            word_tupel = (word, data_scatter, fit_scatter)
            y_dict.update({word: word_tupel})

        scatter_list = []
        for item in y_dict:
            scatter_list.append(y_dict[item][1])
            scatter_list.append(y_dict[item][2])

        py.plot(scatter_list, auto_open=False, filename=filename_path)


    def validate_benchmarks(self, ignore_scales = False):
        """
        Main method for validating which words are good and which ones are bad.
        """

        # Assembling of a combination pair between every keyword inside a scale in the native benchmark
        # The keys correspond to scales, and the values a list containing dictionaries with 2 elements, each element
        # has  { key: keyword, value : average_value }
        combination_full_dict = {}

        if not ignore_scales:
            for id, geo in enumerate(self.benchmark):
                print(self.benchmark[geo])
                for scale, words_dict in self.benchmark[geo].items():
                    combination_scale = list(map(dict, itertools.combinations(words_dict.items(), 2)))
                    combination_full_dict[scale] = combination_scale
                    if len(combination_scale) > 0:
                        all_words = set().union(*(d.keys() for d in combination_scale))
                        approved_words = WordApprover(combination_scale, self.validation_threshold)
                        self.approved_benchmarks.update(set(approved_words.result))
                        self.disapproved_benchmarks.update(all_words.symmetric_difference(set(approved_words.result)))
        else:
            print("test self.benchmark")
            combination_all = list(map(dict, itertools.combinations(self.json_as_dataframe.iloc[0:, 0].items(), 2)))
            print("combinations",len(combination_all))
            combination_full_dict['all'] = combination_all
            if len(combination_all) > 0:
                all_words = set().union(*(d.keys() for d in combination_all))
                print("combinations",len(combination_all))
                approved_words = WordApprover(combination_all, self.validation_threshold)
                self.approved_benchmarks.update(set(approved_words.result))
                self.disapproved_benchmarks.update(all_words.symmetric_difference(set(approved_words.result)))

    def get_approved_words(self):
        return self.approved_benchmarks

    def get_disapproved_words(self):
        return self.disapproved_benchmarks



if __name__ == "__main__":

    validation = BenchmarkValidator('json/benchmarks_US.json')
    validation.validate_benchmarks(True)

    print("Approved Benchmarks: ")
    print(validation.get_approved_words())
    print("Benchmarks to erase from file: ")
    print(validation.get_disapproved_words())
