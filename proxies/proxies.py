import boto3
from time import sleep
class EC2:


    def __init__(self, security_group_id, ami_id, key_pair_name):
        self.ec2 = boto3.resource('ec2')
        self.security_group_id = security_group_id,
        self.ami_id = ami_id,
        self.key_pair_name = key_pair_name
        self.filters = [{'Name': 'tag-key', 'Values': ['test', 'benchmark','proxy', 'swarm']},                  ]
        self.instances = self.filter()


    def filter(self):
        return self.ec2.instances.filter(Filters=self.filters)

    def get_instances(self):
        return self.instances

    def get_instance_by_id(self, id):
        return self.ec2.Instance(id)

    def stop_instances(self):
        try:
            self.instances.stop()
        except:
            pass

    def get_proxy(self, instance):
        try:
            proxy = "https://" + instance.public_ip_address + ":8080"
            return proxy
        except AttributeError:
            print("IP not available for instance. Make sure instance has been started and it is on running state")

    def get_instances_quantity(self):
        return len(list(self.get_instances()))

    def get_instance_name(self, proxy):
        for tag in proxy.tags:
            if 'Name' in tag['Key']:
                name = tag['Value']
                return(name)

    def get_instance_state(self, instance):
        return instance.state['Name']


    def start_instance(self, instance):
        proxy = instance.start()

    def stop_instance(self, instance):
        proxy = instance.stop()

    def get_proxy_by_ip(self, ip):
        """
         Gets proxy instance with the IP given.
        """

        for i in self.instances:
            print("i public address "+ i.public_ip_address)
            print("ip "+ ip)
            if "https://"+ i.public_ip_address +":8080" ==  ip:
                return i
        return None

    def reboot_proxy_by_ip(self, ip):
        """
         Gets Proxy by IP and stops & restarts it.
         :return: New IP-Adress
        """
        instance = self.get_proxy_by_ip(ip)
        self.reboot_proxy(instance)

        return instance.public_ip_address + ":8080"

    def reboot_proxy(self, instance):
        """
         Reboots a single proxy
        """
        print("Rebooting Proxy {}".format(instance.id))
        instance.stop()
        while not instance.state['Name'] == "stopped":
            for i in self.instances:
                if i.id == instance.id:
                    instance = i
            print("Status of {} is {}".format(instance.id, instance.state['Name']))
            sleep(10)
        instance.start()
        while not instance.state['Name'] == "running":
            for i in self.instances:
                if i.id == instance.id:
                    instance = i
            print("Status of {} is {}".format(instance.id, instance.state['Name']))
            sleep(10)

