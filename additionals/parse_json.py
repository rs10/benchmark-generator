# Define GEO here
GEO = "US"

# This file creates a new json file json/benchmarks_GEO_parsed.json with just a single level.

if __name__ == "__main__":
    import json

    with open("../json/benchmarks_{}.json".format(GEO),"r") as fp:
        benchmarks = json.load(fp)

    parsed = {}
    for key in list(benchmarks[GEO]):
        for word in benchmarks[GEO][key]:
            parsed[word] = benchmarks[GEO][key][word]

    with open("../json/benchmarks_{}_parsed.json".format(GEO),"w") as fp:
        json.dump(parsed,fp,indent=2)
