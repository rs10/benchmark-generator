# This file needs to be run with -m from the top level due do importing graphical models as relative path. (python -m additionals.generate_estimations)
# Define GEO here
GEO = "US"

# Define Threshold here
THRESHOLD = 20

# This file creates a new file dataset/dataset_GEO.csv for usage in benchmark_generator.py

if __name__ == "__main__":
    import json, csv

    with open("../json/estimations_{}.json".format(GEO),"r") as fp:
        estimations = json.load(fp)

    with open("../json/benchmarks_{}_parsed.json".format(GEO),"r") as fp:
        benchmarks = json.load(fp)

    # Create lines of dataset_GEO.csv
    HEADER = ["index","estimated_word","truthful_word","estimated_value","DEWFB","percentage_diff","quality"]
    lines = []
    # Iterate over all possible benchmarks
    for word in estimations:
        # Iterate over their different estimations
        for estimator in estimations[word]:
            # Calculate what we need.
            abs_difference_to_benchmark = abs(benchmarks[word]-estimations[word][estimator])
            percentage_difference = abs_difference_to_benchmark / benchmarks[word] * 100
            # Generate line that will be written into csv file
            line = [
                word, # Word that is being estimated
                estimator, # Word used for estimation
                estimations[word][estimator], # Estimated Value using the Benchmark of the Estimator as baseline.
                abs_difference_to_benchmark, # DEWFB in dataset.csv
                round(percentage_difference,2), # % abweichung vom alten Benchmark.
                "GOOD" if percentage_difference < THRESHOLD else "BAD" # kennzeichung der qualität.
            ]
            lines.append(line)
    # Write the csv.
    with open("../dataset/dataset.csv","w",newline="") as fp:
        writer = csv.writer(fp,delimiter=",")
        writer.writerow(HEADER)
        for i,line in enumerate(lines):
            writer.writerow([i]+line)
