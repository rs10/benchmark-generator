# Define GEO you want to cut out of the benchmark.json
GEO = "US"

# This file creates a new json file benchmarks_GEO.json with just that specific geo.

if __name__ == "__main__":
    import json

    with open("../json/benchmarks.json","r") as fp:
        benchmarks = json.load(fp)

    for key in list(benchmarks):
        if key != GEO: del benchmarks[key]

    with open("../json/benchmarks_{}.json".format(GEO),"w") as fp:
        json.dump(benchmarks,fp,indent=2)
