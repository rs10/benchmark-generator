# This file needs to be run with -m from the top level due do importing graphical models as relative path. (python -m additionals.generate_estimations)
# Define GEO here
GEO = "US"

# This file creates a new file json/estimations_GEO.json for usage in additionals/generate_dataset.py

if __name__ == "__main__":
    import json, itertools, time
    from graphical_models.word_approver_markov_chain_model import WordApprover

    with open("json/benchmarks_{}_parsed.json".format(GEO),"r") as fp:
        benchmarks = json.load(fp)

    # Generate pairs of words.
    words = benchmarks.keys()
    pairs = [p for p in itertools.combinations(words,2)]

    # Get Estimation for every single pair.
    # Read the estimations-file
    with open("json/estimations_{}.json".format(GEO),"r") as fp:
        estimations = json.load(fp)
    for pair in pairs:
        # Just a short sleep between requests to not get blocked.
        time.sleep(2)
        # Some Dictionary handling & skipping if we already got a pair.
        if pair[1] not in estimations:
            estimations[pair[1]] = {}
        else:
            # this pair combination is already present.
            if pair[0] in estimations[pair[1]]: continue
        print("Estimating",pair)
        try:
            volume = WordApprover.get_real_volume(pair,benchmarks[pair[0]],GEO)
            estimations[pair[1]][pair[0]] = volume
        except:
            # If we error out, we are blocked.
            # So we save the estimations we have so far
            with open("json/estimations_{}.json".format(GEO),"w") as fp:
                json.dump(estimations,fp)
            # and exit the script for later return
            exit()

    # Save estimations anyway, even though we can work with the dictionary.
    with open("json/estimations_{}.json".format(GEO),"w") as fp:
        json.dump(estimations,fp)