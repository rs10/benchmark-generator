# Define GEO here
GEO = "US"
# Copy Set of disapproved words here
SET = {'you', 'body', 'Cannabis', 'my', 'Preis', 'Sony Xperia', 'Download', 'kohl'}

# This file cleans up benchmark_GEO.json from the disapproved words.

if __name__ == "__main__":
    import json

    with open("../json/benchmarks_{}.json".format(GEO),"r") as fp:
        benchmarks = json.load(fp)

    # Iterate over disapproved
    for word in SET:
        # Iterate through benchmarking file structure
        for key in list(benchmarks[GEO]):
            if word in benchmarks[GEO][key]:
                # Delete disapproved if present
                del benchmarks[GEO][key][word]

    with open("../json/benchmarks_{}.json".format(GEO),"w") as fp:
        json.dump(benchmarks,fp,indent=2)
