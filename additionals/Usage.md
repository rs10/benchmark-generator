# Usage Documentation of additionals to prepare calling the benchmark generator.

To generate the benchmarks for a specific geo starting with an old benchmark json:

1. Run additionals/cut_geo.py following the comment instructions
2. Run benchmark_validator.py with that file specified 
3. Run additionals/delete_disapproved.py following the comment instructions.
4. Run additionals/parse_json.py following the comment instructions.
5. Run additionals/generate_estimations.py following the comment instructions
6. Run additionals/generate_dataset.py following the comment instructions
7. Run benchmark_generator.py after changing the hardcoded GEO's...