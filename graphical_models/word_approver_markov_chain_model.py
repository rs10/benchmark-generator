from pytrends.request import TrendReq
from pytrends.exceptions import ResponseError
from termcolor import colored
from helper.utils import first, make_ordered_dict_in_list, get_values_in_list_of_dicts
from google_trends.google_trends_wrapper import GoogleTrends
import random, json



class WordApprover :
    """
    Class for validating current benchmarks to check how reliable the words still are.
    In particular, it approves the volume calculated for the second word in a pair comparison, assuming first word
    volume to be true. In case the volume is not approved, it considers the possibility that first word value might
    not be truthful.

    """

    def __init__(self, list_all_pairings, threshold = 0.2):
        """

        :param

        - list_words: initial state with all the possible comparisons to be made. Every element in the list
        is a dictionary in the format dict { word : value }
        - threshold: the percentage on which the calculated volume can differ to the one state on the benchmark

        """
        if len(list_all_pairings) > 0:
            self.list_all_pairings = make_ordered_dict_in_list(list_all_pairings)
            self.original_pairings = self.list_all_pairings.copy()
            self.last_word = { next(reversed(self.list_all_pairings[-1])) :
                               next(reversed(self.list_all_pairings[-1].values())) }
            self.previous_state = ''
            self.current_state = ''
            self.approval_threshold = threshold
            self.result = []
            self.run(self.list_all_pairings, self.get_initial_words(), self.get_initial_values(), self.previous_state)


    def get_initial_words(self):
        initial_pair = self.list_all_pairings[0] if len(self.list_all_pairings) else None

        if initial_pair is not None:
            return tuple(list(initial_pair.keys()))
        else:
            return None

    def get_initial_values(self):
        initial_pair = self.list_all_pairings[0] if len(self.list_all_pairings) else None
        if initial_pair is not None:
            return tuple(list(initial_pair.values()))
        else:
            return None

    def remove_any_word_from_words_list(self, words_list, tuple_words):

        # we need to use the slice notation [:] here because we are modifying the list while iterating it
        for pair in words_list[:]:
            if any([i in first(pair) for i in tuple_words]):
                del words_list[0]

        return words_list

    @staticmethod
    def get_real_volume(pair_words, truthful_value, geo, timeframe='today 5-y', use_proxies=False):
        """

           :param

           - pair_words: the 2 words on which to do the association
           - truthful_value: a truthful value for the average of interest ASSOCIATED TO THE 1st KEYWORD in pair_keywords
           - geo : The location for the Google Trends search
           - timeframe : The timeframe for the search as per Google Trends specification (default for last 5 years)

            :return
            - Estimated average volume for the 2nd keyword in pair_words
        """
        try:

            gt = GoogleTrends.get_google_trends_data(keyword_list=pair_words, geo=geo, use_proxies=use_proxies)
            interest = GoogleTrends.interest_over_time(gt)

            average_pair = interest[list(pair_words)].mean()
            if average_pair[pair_words[0]] > 0:
                volume_avg = round(truthful_value * average_pair[pair_words[1]] / average_pair[pair_words[0]])
            else:
                volume_avg = 0
                
            return volume_avg
        except ResponseError:

            #we probably should keep a list of the ones who couldn't be verified because of pytrends error
            print("Response Error. Passing...")
            pass

    def word_pass_diff_threshold(self, native_volume, new_volume):
        percentage_of_truthful = round(100 * new_volume / native_volume, 2)
        percentage_diff = round(abs(100 - percentage_of_truthful), 2)

        # higher than threshold
        if percentage_diff > self.approval_threshold * 100:
            return False
        else:
            return True

    def run(self, remaining_words, pair_words, native_values, previous_state=''):
        """
        This Method puts the Markov Model in motion
            Input Args:
                - list_words: A list with the remaining words to be checked.

                - tuple_comparison_pair_words:  A tuple with the words to be compared. The order on which the words are
                placed in the tuple matters, as the first word is assumed truthful by this class

                - tuple_comparison_pair_values : The tuple of values referring to tuple_comparison_pair_words. Again,
                order matters

                previous_state: Can accept 3 values:
                    '' : initial state
                    'P': Pass state
                    'F': Fail state

            Returns:
                - a list of the approved words for this run

        """

        if(len(remaining_words) > 0 ):

            # we are always trying to validate the 1st word in the pair!
            truthful_value = native_values[0]

            # but we do that by checking if the volume of 2nd keyword matched with its own native value
            comparable_value = native_values[1]

            volume_word2 = self.get_real_volume(pair_words, truthful_value, geo='US')
            word_is_healthy = self.word_pass_diff_threshold(comparable_value, volume_word2)

            if not word_is_healthy:

                # first fail state
                if(previous_state == ''):
                    inversed_pair_words = pair_words[::-1]
                    inversed_native_values = native_values[::-1]
                    state = 'F'
                    self.run(remaining_words, inversed_pair_words, inversed_native_values, state)

                # second fail state (both words are bad: remove all entries from the full list that contains either word)
                # Omit the info about previous state, since this end the cycle for these words
                if(previous_state == 'F'):
                    print(colored("Pair comparison {} gave a fail result for both words"
                                  .format(pair_words), 'red'))
                    remaining_words = self.remove_any_word_from_words_list(remaining_words, pair_words)
                    self.run(remaining_words, self.get_initial_words(), self.get_initial_values())

            else:
                # Both words are good. Add them to the list of result and remove all entries from the full list that
                # contains either word
                if (previous_state == ''):
                    print(colored("Pair comparison {} gave a passable result for both words"
                                  .format(pair_words), 'green'))
                    self.result.extend(list(pair_words))
                    remaining_words = self.remove_any_word_from_words_list(remaining_words, pair_words)

                    self.run(remaining_words, self.get_initial_words(), self.get_initial_values())

                # The first word was bad, but the second one is good. Discard only the first word from all entries and
                # add the second to the result list
                if (previous_state == 'F'):
                    # remember in this recursion the pair has been inverted in previous state, so now the 2nd word
                    # in the tuple is the passing one
                    passable_word = pair_words[0]
                    failed_word = pair_words[1]

                    print(colored("Pair comparison {} gave a passable result for word {}, but it failed for word {}"
                                  .format(pair_words, passable_word, failed_word), 'magenta'))
                    self.result.append(passable_word)

                    for item in remaining_words[:]:
                        if failed_word == first(item):
                            del remaining_words[0]

                    self.run(remaining_words, self.get_initial_words(), self.get_initial_values() )
        else:
            # the last word from the benchmark will be the last word in the initial list_all_pairings list.
            # That word never gets to be checked as a truthful one, so we need to manually do it, once remaining_words
            # is empty. We can use a previous 'P' word to do it in one step and make sure to use the last word as first
            # in the pair.
            # In one specific scenario the word might have been already checked: first truthful word failed, but the 2nd
            # did not, and it happens that the 2nd word is exactly the last one in initial list_all_pairings. We can
            # simply check it is already in the result list

            last_word = list(self.last_word.keys())[0]
            last_word_value = list(self.last_word.values())[0]

            if last_word not in self.result:
                # if we have any results so far, use any of it as the 2nd word in pair
                if(len(self.result) > 0):
                    comparable_word = random.choice(self.result)
                    comparable_value = random.choice(get_values_in_list_of_dicts(self.original_pairings, comparable_word,
                                                                                 False))
                #otherwise the last word takes the role of truthful word
                else:
                    comparable_word = list(self.original_pairings[-1].keys())[0]
                    comparable_value = list(self.original_pairings[-1].values())[0]

                pair_words = [last_word, comparable_word]

                volume_word2 = self.get_real_volume(pair_words, last_word_value, geo='US')
                word_is_healthy = self.word_pass_diff_threshold(comparable_value, volume_word2)

                if word_is_healthy:
                    print(colored("Pair comparison {} gave a passable result for both words"
                                  .format(pair_words), 'green'))
                    self.result.append(pair_words[0])

            return self.result


#geo = 'US'
#combination = [{'wikipedia': 685000, 'chase': 850000}, {'wikipedia': 685000, 'nytimes': 426625}, {'wikipedia': 685000, 'aliexpress': 560000}, {'wikipedia': 685000, 'imgur': 290375}, {'chase': 850000, 'nytimes': 426625}, {'chase': 850000, 'aliexpress': 560000}, {'chase': 850000, 'imgur': 290375}, {'nytimes': 426625, 'aliexpress': 560000}, {'nytimes': 426625, 'imgur': 290375}, {'aliexpress': 560000, 'imgur': 290375}]



#test = WordApprover(combination, threshold=0.85)

#print("The following words are still good benchmarks: ")
#print(colored(test.result,'blue'))
