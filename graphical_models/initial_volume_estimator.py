from graphical_models.word_approver_markov_chain_model import WordApprover
import pandas as pd
import numpy as np
import itertools
import json
from helper.utils import read_json

#Estimator will work on a list just like combination_all
class Estimator :

    def __init__(self, benchmark_words):
        self.benchmark_values = benchmark_words
        self.estimator_table = pd.DataFrame()


    def build_combinations_list(self, benchmark_dictionary):

        #The 2 below are comented because they generate permutations without repetition, while what we
        # really need is the cartesian product (which includes repetitions
        #self.combination_all_values = list(itertools.combinations(benchmark_dictionary.values(), 2))
        #self.combination_all_keys = list(itertools.combinations(benchmark_dictionary, 2))

        self.combination_all_keys = [p for p in itertools.product(benchmark_dictionary, repeat=2)]
        self.combination_all_values = [p for p in itertools.product(benchmark_dictionary.values(), repeat=2)]
        print(self.combination_all_keys)
        return (self.combination_all_keys, self.combination_all_values)

    def build_estimators(self):
        used_words = []


        for word in self.benchmark_values.keys():
            if word not in used_words:
                tuples_filtered = [tup for tup in set(self.combination_all_keys) if word in tup[0]]
                used_words.append(word)
                row = self.build_row_estimator(word, tuples_filtered)
                self.estimator_table = pd.concat([self.estimator_table, row], axis=0)

        print("Estimation table")
        print(self.estimator_table)

        self.estimator_table.to_csv(r'../csv/full_benchmark _estimators_2.csv', index=True)


    def build_row_estimator(self, word, pairwise_tuples):

        columns = [a_tuple[1] for a_tuple in pairwise_tuples]
        size = len(columns)
        estimation_row = pd.DataFrame(np.empty((1,size),dtype=pd.Float64Index), index = [word], columns = columns)

        for tup in pairwise_tuples:
            if not tup[0] == tup[1]:

                estimation_row.loc[tup[0],tup[1]] = WordApprover.get_real_volume(
                    pair_words=list(tup),
                    truthful_value=self.benchmark_values[word],
                    geo='US',
                    use_proxies= True
                )
        print("\n\n\n")
        print(estimation_row)
        #estimation_row.to_csv(r'../csv/'+word+'_estimators.csv', index=True)

        return estimation_row

    def read_estimator_table(self, file):
        self.estimator_table = pd.read_csv(file, index_col=0, header=0)

    def add_truthful_values_column(self):
        size = len(self.benchmark_values)
        truthful_column = pd.DataFrame(np.empty((size, 1), dtype=pd.Float64Index), index=self.benchmark_values.keys(), columns=['BENCHMARK_VALUE'])

        for word in self.benchmark_values.keys():
            truthful_column.loc[word, 'BENCHMARK_VALUE'] = self.benchmark_values[word]

        print(self.estimator_table)
        self.estimator_table = pd.concat([self.estimator_table, truthful_column], axis=1)
        print("Foe")
        print(self.estimator_table)

    def save_estimator_table_to_file(self, file):
        self.estimator_table.to_csv(file, index=True)

    def get_estimator(self):
        return self.estimator_table


if __name__ == "__main__":
    benchmark_values = read_json('../json/us.json')
    print(json.dumps(benchmark_values, indent=4, sort_keys=False))

    estimator = Estimator(benchmark_values)
    estimator.read_estimator_table('../csv/full_benchmark_estimators_2.csv')
    estimator.add_truthful_values_column()
    estimator.save_estimator_table_to_file(r'../csv/benchmark_matrix.csv')
